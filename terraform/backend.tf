provider "aws" {
  region = "eu-west-1"
}

terraform {
  backend "s3" {
    bucket = "sehyoo96-node-aws-jenkins-terraform"
    key = "node-aws-jenkins-terraform.tfstate"
    region = "eu-west-1"
  }
}
